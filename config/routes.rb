PathsCc::Application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  namespace :backend do
    resources :monuments
    resources :users
    resources :sessions, only: :create

    root to: 'dashboard#show'
    get 'login' => 'sessions#new', :as => :login
    get 'logout' => 'sessions#destroy', :as => :logout
  end

  root to: 'homepage#show'
end
