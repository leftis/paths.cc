require "bundler/capistrano"

set :bundle_flags, "--deployment --quiet --binstubs"
set (:bundle_cmd) { "#{release_path}/bin/bundle" }

load 'deploy/assets'

set :rake, "bundle exec rake"

set :application, "paths.cc"
set :repository,  "git@bitbucket.org:ryudo/paths.cc.git"
set :user, 'paths_cc'
set :scm, 'git'
set :branch,          "master"
set :deploy_to, "/home/#{user}/#{application}"
set :deploy_via,      :remote_cache
set :use_sudo,        false
set :keep_releases,   3

set :default_environment, {
  'PATH' => "$HOME/.rbenv/shims:$HOME/.rbenv/bin:$PATH"
}

role :app, application
role :web, application
role :db,  application, :primary => true

# before "deploy:setup", "db:configure"

namespace :db do
  desc "Create database yaml in shared path"
  task :configure do
    set :database_username do
      "root"
    end

    set :database_password do
      Capistrano::CLI.password_prompt "Database Password: "
    end

    db_config = <<-EOF
      base: &base
        adapter: mysql2
        encoding: utf8
        reconnect: false
        pool: 5
        username: #{database_username}
        password: #{database_password}

      development:
        database: #{application}_development
        <<: *base

      test:
        database: #{application}_test
        <<: *base

      production:
        database: #{application}_production
        <<: *base
    EOF

    run "mkdir -p #{shared_path}/config"
    put db_config, "#{shared_path}/config/database.yml"
  end
end