module Translatable
  extend ActiveSupport::Concern
  included do
    has_many :translations
    attr_accessible :translations_attributes
    accepts_nested_attributes_for :translations, allow_destroy: true
  end
end