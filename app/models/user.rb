class User < ActiveRecord::Base
  # Sorcery
  authenticates_with_sorcery!
  
  # Validations
  validates :email,                   presence: true, uniqueness: true
  validates :username,                presence: true, uniqueness: true
  validates :password,                presence: true, confirmation: true, length: { minimum: 3}
  validates :password_confirmation,   presence: true
  
  # Associations
  has_many :monuments
end
