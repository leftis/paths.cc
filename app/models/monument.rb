class Monument < ActiveRecord::Base

  # Translations
  translates :name, :title, :description, versioning: true

  # Associations
  belongs_to :user

  class Translation
    attr_accessible :locale
  end

  def to_s
    name
  end
end