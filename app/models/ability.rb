class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
      user ||= User.new # guest user (not logged in)
      if user.is_a? Admin
        can :manage, :all
        can :manage, :monuments
        can :manage, :monument_translations
        can :access, :ckeditor
      else
        can :read, :all
      end
  end
end
