class Backend::ApplicationController < ActionController::Base
  layout "backend"
  protect_from_forgery with: :exception
  before_filter :require_login, except: :not_authenticated
  helper_method :current_users_list
  inherit_resources
  respond_to :html, :json

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  protected
    def not_authenticated
      redirect_to backend_login_path, :alert => "Please login first."
    end

    def current_users_list
      current_users.map {|u| u.email}.join(", ")
    end
end