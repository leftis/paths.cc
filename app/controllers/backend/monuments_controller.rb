class Backend::MonumentsController < Backend::ApplicationController
  respond_to :html, :json
  load_and_authorize_resource

  public
  def permitted_params
    params.permit(monument: [:user_id, :name, :title, :description, :locale])
  end
end