class Backend::SessionsController < Backend::ApplicationController
  before_action :login_user,    only: :create
  before_action :require_login, only: :destroy
  layout 'sessions'
  respond_to :html, :js, :xml

  def new
  end

  def create
    if @user
      redirect_back_or_to backend_root_url, notice: 'Logged in!'
    else
      redirect_to backend_login_url, alert: 'Email or password was invalid.'
    end
  end

  def destroy
    logout
    redirect_to root_path, notice: 'Successfully logged out.'
  end

  private
    def login_user
      @user = login(params[:session][:email], params[:session][:password])
    end

    def session_params
      params.require(:user).permit(:email, :password, :passowrd_confirmation)
    end
end