class CreateTableMonuments < ActiveRecord::Migration
  
  def up
    create_table :monuments do |t|
      t.integer :user_id, null: false
      t.timestamps
    end
    
    Monument.create_translation_table!(name: :string, title: :string, description: :text)
  end
  
  def down
    drop_table :monuments
    Monument.drop_translation_table!
  end
  
end
